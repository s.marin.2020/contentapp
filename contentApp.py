from webApp import webApp

BASE_PAGE = """
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Práctica contentApp</title>
    </head>
    <body>
        {}
    </body>
</html>
"""

RAIZ = """<h2>Bienvenido a contentApp</h2>
    <p>Los recursos que tienes disponibles son:</p>
    <ul>
        {}
    </ul>"""

NOT_FOUND = '<h2>Recurso "{}" no disponible</h2>'


class contentApp(webApp):
    content = {'/': RAIZ, '/hola': "<h2>Hola ingenieros</h2>", '/adios': "<h2>¡¡Hasta la próxima!!</h2>",
               '/como_vas':"<p>Un poco nervioso</p>"}

    def showContent(self):
        r = ''
        for k in self.content:
                r += f"<li>{k}</li>"
        return r

    def parse(self, request):
        resource = request.split(" ", 2)[1]
        return resource

    def process(self, parsedRequest):
        k = self.showContent()
        if parsedRequest in self.content:
            if parsedRequest == '/':
                self.content[parsedRequest] = RAIZ.format(k)
            htmlAnswer = BASE_PAGE.format(self.content[parsedRequest])
            returnCode = "200 OK"
        else:
            returnCode = "404 Resource Not Found"
            notFound = NOT_FOUND.format(parsedRequest)
            htmlAnswer = BASE_PAGE.format(notFound)
        return returnCode, htmlAnswer


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
